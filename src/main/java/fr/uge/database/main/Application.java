package fr.uge.database.main;


import fr.uge.database.CreateDB;
import fr.uge.database.HistoryService;
import fr.uge.database.TrackService;

import java.util.UUID;

public class Application {
    public static void main(String[] args) {
        var create = new CreateDB();
        create.connect();
        /*create.whenCreatingAKeyspace_thenCreated();
        create.createAllTable();
        create.insertValues();*/
        var track = new TrackService(create.getSession());
        System.out.println(track.trackService("17ef5058-2e4f-4abd-8052-0a39e0daf7e6"));

        var history = new HistoryService(create.getSession());
        System.out.println(history.getHistory(UUID.fromString("02a7ef03-de18-4c19-a349-db4eac46e197"),Long.valueOf("1636409703525")));





        create.close();
    }
}
