package fr.uge.database.object;

import com.datastax.driver.core.ResultSet;
import com.datastax.driver.core.Row;
import com.datastax.driver.core.Session;
import com.fasterxml.jackson.databind.JsonNode;
import fr.uge.database.KafkaManager;
import fr.uge.database.repository.ParentRepository;
import fr.uge.database.table.Alert;
import fr.uge.database.table.Position;
import fr.uge.database.table.User;
import org.apache.kafka.clients.consumer.ConsumerRecord;

import java.io.IOException;
import java.util.Map;

public class AlertingService {
    private static final String ALERT_TOPIC_OUT = "alertKafkaOut";

    public void process(ConsumerRecord<String, JsonNode> record, Session session) throws IOException {
        var token = record.key();
        var jsonNode = record.value();
        var array = jsonNode.get("coordinates");
        double coords[] = {array.get(1).asDouble(), array.get(2).asDouble()};
        long timestamp = jsonNode.get("timestamp").asLong();
        String reason = jsonNode.get("reason").asText();

        var pos = new Position(coords);

        // user_service API reponse
        StringBuilder sb = new StringBuilder("SELECT * FROM ")
                .append("service.User")
                .append("WHERE ").append("user_token = ").append(record.key());
        String query = sb.toString();
        ResultSet resultSet = session.execute(query);
        Row row = resultSet.one();
        
        var user = new User(row.getString(("user_token")),row.getString("name"));
        var alert = new Alert(reason, pos.getPosition_id(), user.getUser_id(), timestamp, session);

        var parentRepository = new ParentRepository(session);
        var parents = parentRepository.getFromId(user.getUser_id());
        for(var parent : parents) {
            KafkaManager.ProducterAlert(ALERT_TOPIC_OUT, alert, parent);
        }
    }
}
