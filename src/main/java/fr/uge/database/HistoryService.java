package fr.uge.database;

import com.datastax.driver.core.ResultSet;
import com.datastax.driver.core.Row;
import com.datastax.driver.core.Session;

import java.util.Arrays;
import java.util.UUID;

public class HistoryService {

    private final String KEYSPACE_NAME = "service";
    private final Session session;

    public HistoryService(Session session) {
        this.session = session;
    }

    public String getHistory(UUID user_id, long timestamp_start) {
        return getHistory(user_id, timestamp_start, System.currentTimeMillis());
    }

    public String getHistory(UUID user_id, long timestamp_start, long timestamp_end) {
        return getHistory(user_id, timestamp_start, timestamp_end, 100);
    }

    public String getHistory(UUID user_id, long timestamp_start, long timestamp_end, int count) {
        if(count > 1000 || count < 0) {
            throw new IllegalArgumentException("Wrong number of messages, should be between 0 and 1000 and is " + count);
        }
        StringBuilder result = new StringBuilder();

        String query = "SELECT * FROM " +
                KEYSPACE_NAME + "." + "Alert" +
                " WHERE " + "from_id = " + user_id +
        " AND " + "server_timestamp >= " + timestamp_start +
                " AND " + "server_timestamp < " + timestamp_end +
                " AND reason = 'ZONEOUT'"
                + " LIMIT " + count +" ALLOW FILTERING;";
        ResultSet resultSet = session.execute(query);

        String sb1 = "SELECT * FROM " +
                KEYSPACE_NAME + "." + "User" +
                " WHERE " + "user_id = " + user_id;
        ResultSet resultSet1 = session.execute(sb1);

        String user_name = resultSet1.one().getString("name");

        for (Row row : resultSet) {

            String query2 = "SELECT * FROM " +
                    KEYSPACE_NAME + "." + "Position" +
                    " WHERE " + "position_id = " + row.getObject("position_id");
            ResultSet resultSet2 = session.execute(query2);
            Row row2 = resultSet2.one();

            result.append("user_id : ").append(user_id).append(", ")
                    .append("user_name : ").append(user_name).append(", ")
                    .append("timestamp : ").append(row.getLong("timestamp")).append(", ")
                    .append("server_timestamp : ").append(row.getLong("server_timestamp")).append(", ")
                    .append("coordinates : ").append(row2.getDouble("coordinates_x")).append(",").append(row2.getDouble("coordinates_y"))
                    .append(", reason : ").append(row.getString("reason"))
                    .append(";\n");
        }


        if(!result.isEmpty()) {
            return "200 OK : " + result;
        } else {
            return "404 NOT FOUND";
        }
    }

}