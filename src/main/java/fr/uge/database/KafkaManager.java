package fr.uge.database;

import com.datastax.driver.core.Session;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ObjectNode;
import fr.uge.database.object.AlertingService;
import fr.uge.database.table.Alert;
import fr.uge.database.table.User;
import org.apache.kafka.clients.consumer.ConsumerConfig;
import org.apache.kafka.clients.consumer.ConsumerRecords;
import org.apache.kafka.clients.consumer.KafkaConsumer;
import org.apache.kafka.clients.producer.KafkaProducer;
import org.apache.kafka.clients.producer.ProducerConfig;
import org.apache.kafka.clients.producer.ProducerRecord;
import org.apache.kafka.common.KafkaException;
import org.apache.kafka.common.serialization.StringDeserializer;
import org.apache.kafka.common.serialization.UUIDSerializer;
import org.apache.kafka.connect.json.JsonDeserializer;
import org.apache.kafka.connect.json.JsonSerializer;

import java.io.IOException;
import java.time.Duration;
import java.util.*;

public class KafkaManager {
    private static final Properties props, props2;
    private static final AlertingService alertingService;
    static {
        alertingService = new AlertingService();
        props = new Properties();
        props.put(ProducerConfig.CLIENT_ID_CONFIG, "AlertService");
        props.put(ProducerConfig.BOOTSTRAP_SERVERS_CONFIG, "localhost:9092");
        props.put(ProducerConfig.KEY_SERIALIZER_CLASS_CONFIG, UUIDSerializer.class.getName());
        props.put(ProducerConfig.VALUE_SERIALIZER_CLASS_CONFIG, JsonSerializer.class.getName());
        props2 = new Properties();
        props2.put(ConsumerConfig.CLIENT_ID_CONFIG, "AlertService");
        props2.put(ConsumerConfig.BOOTSTRAP_SERVERS_CONFIG, "localhost:9092");
        props2.put(ConsumerConfig.KEY_DESERIALIZER_CLASS_CONFIG, StringDeserializer.class.getName());
        props2.put(ConsumerConfig.VALUE_DESERIALIZER_CLASS_CONFIG, JsonDeserializer.class.getName());
        props2.put(ConsumerConfig.GROUP_ID_CONFIG, "second_app");
        props2.put(ConsumerConfig.AUTO_OFFSET_RESET_CONFIG,"earliest");

    }
    public static void ProducterAlert(String topicName, Alert alert, User userParent) throws IOException {
        ObjectMapper mapper = new ObjectMapper();
        ObjectNode jNode = mapper.createObjectNode();
        jNode.put("to_id",alert.getUser().getUser_id().toString());
        jNode.put("to_name",alert.getUser().getName());
        jNode.put("from_id",userParent.getUser_id().toString());
        jNode.put("from_name",userParent.getName());
        jNode.put("timestamp",alert.getTimestamp());
        jNode.put("server_timestamp",alert.getServerTimestamp());
        jNode.put("reason",alert.getReason());
        var tmp= mapper.readTree(jNode.toString());
        try (KafkaProducer<UUID, JsonNode> producer = new KafkaProducer<>(props)) {
            System.out.println("Start sending messages ...");
            producer.send(new ProducerRecord<>(topicName, alert.getAlert_id(), tmp ));
        } catch (KafkaException e) {
            System.exit(-1);
        }
    }

    public static void ConsumerAlert(String topicName, Session session) {
        System.out.println("ConsumerConfig terminé");

        KafkaConsumer<String, JsonNode> consumer = new KafkaConsumer<>(props2);
        consumer.subscribe(Collections.singletonList(topicName));
        ConsumerRecords<String, JsonNode> consumerRecords = consumer.poll(Duration.ofMillis(100));
        consumerRecords.forEach(c -> {
            //process
            try {
                alertingService.process(c, session);
            } catch (IOException e) {
                e.printStackTrace();
            }
            System.out.println("Received - key : " + c.key() + ", value : " + c.value());
        });
    }
}
