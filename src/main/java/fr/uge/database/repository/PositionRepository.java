package fr.uge.database.repository;

import com.datastax.driver.core.ResultSet;
import com.datastax.driver.core.Row;
import com.datastax.driver.core.Session;
import fr.uge.database.table.*;

import java.util.Optional;
import java.util.UUID;

public class PositionRepository {
    private static final String TABLE_NAME = "Position";
    private final String KEYSPACE_NAME = "service";
    private Session session;

    public PositionRepository(Session session){
        this.session = session;
    }

    public void insert(Position pos) {
            StringBuilder sb = new StringBuilder("INSERT INTO ")
                    .append(KEYSPACE_NAME).append(".").append(TABLE_NAME)
                    .append("(position_id, coordinates_x, coordinates_y) ")
                    .append("VALUES (")
                    .append(pos.getPosition_id()).append(", ")
                    .append(pos.getCoordinates()[0]).append(",")
                    .append(pos.getCoordinates()[1]).append(");");

            String query = sb.toString();
            session.execute(query);
        }

    public void createTable() {
        StringBuilder sb = new StringBuilder("CREATE TABLE IF NOT EXISTS ")
                .append(KEYSPACE_NAME).append(".")
                .append(TABLE_NAME).append("(")
                .append("position_id uuid PRIMARY KEY, ")
                .append("coordinates_x double, ")
                .append("coordinates_y double);");

        String query = sb.toString();
        session.execute(query);
    }

    public Optional<Position> getFromId(UUID id){
        StringBuilder sb = new StringBuilder("SELECT * FROM ")
                .append(KEYSPACE_NAME).append(".").append(TABLE_NAME)
                .append(" WHERE ").append("position_id = ").append(id);
        String query = sb.toString();
        ResultSet resultSet = session.execute(query);
        Row row = resultSet.one();
        if(row == null){
            return Optional.empty();
        }else{
            double coord[] = {row.getDouble("coordinates_x"),row.getDouble("coordinates_y")};
            return Optional.of(new Position(row.getUUID("position_id"),coord));
        }
    }
}
