package fr.uge.database.repository;

import com.datastax.driver.core.ResultSet;
import com.datastax.driver.core.Row;
import com.datastax.driver.core.Session;
import fr.uge.database.table.User;

import java.util.Optional;
import java.util.UUID;

public class UserRepository {
    private static final String TABLE_NAME = "User";
    private final String KEYSPACE_NAME = "service";
    private Session session;

    public UserRepository(Session session){
        this.session = session;
    }

    public void insert(User user) {
        StringBuilder sb = new StringBuilder("INSERT INTO ")
                .append(KEYSPACE_NAME).append(".").append(TABLE_NAME).append("(user_id, user_token, name) ")
                .append("VALUES (").append(user.getUser_id()).append(", '").append(user.getUser_token()).append("','").append(user.getName()).append("');");

        String query = sb.toString();
        session.execute(query);
    }

    public void createTable() {
        StringBuilder sb = new StringBuilder("CREATE TABLE IF NOT EXISTS ")
                .append(KEYSPACE_NAME).append(".")
                .append(TABLE_NAME).append("(")
                .append("user_id uuid PRIMARY KEY, ")
                .append("user_token text, ")
                .append("name text);");

        String query = sb.toString();
        session.execute(query);
    }

    public Optional<User> getFromId(UUID id){
        StringBuilder sb = new StringBuilder("SELECT * FROM ")
                .append(KEYSPACE_NAME).append(".").append(TABLE_NAME)
                .append(" WHERE ").append("user_id = ").append(id);
        String query = sb.toString();
        ResultSet resultSet = session.execute(query);
        Row row = resultSet.one();
        if(row == null){
            return Optional.empty();
        }else{
            return Optional.of(new User(row.getUUID("user_id"),row.getString(("user_token")),row.getString("name")));
        }
    }
}
