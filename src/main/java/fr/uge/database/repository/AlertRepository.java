package fr.uge.database.repository;

import com.datastax.driver.core.ResultSet;
import com.datastax.driver.core.Row;
import com.datastax.driver.core.Session;
import fr.uge.database.table.Alert;
import fr.uge.database.table.User;

import java.util.Optional;
import java.util.UUID;

public class AlertRepository {
    private static final String TABLE_NAME = "Alert";
    private final String KEYSPACE_NAME = "service";
    private Session session;

    public AlertRepository(Session session){
        this.session = session;
    }

    public void insert(Alert alert) {
        StringBuilder sb = new StringBuilder("INSERT INTO ")
                .append(KEYSPACE_NAME).append(".").append(TABLE_NAME)
                .append("(alert_id, reason, position_id, from_id, timestamp, server_timestamp) ")
                .append("VALUES (")
                .append(alert.getAlert_id()).append(", '")
                .append(alert.getReason()).append("',")
                .append(alert.getPosition().getPosition_id()).append(",")
                .append(alert.getUser().getUser_id()).append(",")
                .append(alert.getTimestamp()).append(",")
                .append(alert.getServerTimestamp()).append(");");

        String query = sb.toString();
        session.execute(query);
    }

    public void createTable() {
        StringBuilder sb = new StringBuilder("CREATE TABLE IF NOT EXISTS ")
                .append(KEYSPACE_NAME).append(".")
                .append(TABLE_NAME).append("(")
                .append("alert_id uuid, ")
                .append("reason text, ")
                .append("position_id uuid, ")
                .append("from_id uuid, ")
                .append("timestamp bigint, ")
                .append("server_timestamp bigint,")
                .append("PRIMARY KEY ((alert_id),server_timestamp,reason))")
                .append("WITH CLUSTERING ORDER BY (server_timestamp DESC);");

        String query = sb.toString();
        session.execute(query);
    }

    public Optional<Alert> getFromId(UUID id){
        StringBuilder sb = new StringBuilder("SELECT * FROM ")
                .append(KEYSPACE_NAME).append(".").append(TABLE_NAME)
                .append("WHERE ").append("alert_id = ").append(id);

        String query = sb.toString();
        ResultSet resultSet = session.execute(query);
        Row row = resultSet.one();
        if(row == null){
            return Optional.empty();
        }else{
            return Optional.of(new Alert(row.getUUID("alert_id"),row.getString("reason"),row.getUUID("position_id"),row.getUUID("from_id"),row.getLong("timestamp"),session));
        }
    }
}
