package fr.uge.database.repository;

import com.datastax.driver.core.Session;
import fr.uge.database.table.Message;

public class MessageRepository {
    private static final String TABLE_NAME = "Message";
    private final String KEYSPACE_NAME = "service";
    private Session session;

    public MessageRepository(Session session){
        this.session = session;
    }

    public void insert(Message msg) {
        StringBuilder sb = new StringBuilder("INSERT INTO ")
                .append(KEYSPACE_NAME).append(".").append(TABLE_NAME)
                .append("(message_id, message, position_id, from_id, timestamp, server_timestamp) ")
                .append("VALUES (")
                .append(msg.getMessage_id()).append(", '")
                .append(msg.getMessage()).append("',")
                .append(msg.getPosition_id()).append("',")
                .append(msg.getFrom_id()).append("',")
                .append(msg.getTimestamp()).append("',")
                .append(msg.getServerTimestamp()).append(");");


        String query = sb.toString();
        session.execute(query);
    }

    public void createTable() {
        StringBuilder sb = new StringBuilder("CREATE TABLE IF NOT EXISTS ")
                .append(KEYSPACE_NAME).append(".")
                .append(TABLE_NAME).append("(")
                .append("message_id uuid PRIMARY KEY, ")
                .append("message text, ")
                .append("position_id uuid, ")
                .append("from_id uuid, ")
                .append("timestamp bigint, ")
                .append("server_timestamp bigint);");


        String query = sb.toString();
        session.execute(query);
    }
}
