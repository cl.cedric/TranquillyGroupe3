package fr.uge.database.repository;

import com.datastax.driver.core.ResultSet;
import com.datastax.driver.core.Session;
import fr.uge.database.table.Parent;
import fr.uge.database.table.User;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

public class ParentRepository {
    private static int id = 1;
    private static final String TABLE_NAME = "Parent";
    private final String KEYSPACE_NAME = "service";
    private Session session;

    public ParentRepository(Session session){
        this.session = session;
    }

    public void insert(Parent parent) {
        for(var parent_id:parent.getParent_id()){
            StringBuilder sb = new StringBuilder("INSERT INTO ")
                    .append(KEYSPACE_NAME).append(".").append(TABLE_NAME).append("(id,enfant_id, parent_id) ")
                    .append("VALUES (").append(id).append(",").append(parent.getEnfant_id()).append(",").append(parent_id).append(");");

            String query = sb.toString();
            id++;
            session.execute(query);
        }
    }

    public void createTable() {
        StringBuilder sb = new StringBuilder("CREATE TABLE IF NOT EXISTS ")
                .append(KEYSPACE_NAME).append(".")
                .append(TABLE_NAME).append("(")
                .append("id int PRIMARY KEY,")
                .append("parent_id uuid, ")
                .append("enfant_id uuid);");

        String query = sb.toString();
        session.execute(query);
    }

    public List<User> getFromId(UUID id){
        StringBuilder sb = new StringBuilder("SELECT * FROM ")
                .append(KEYSPACE_NAME).append(".").append(TABLE_NAME)
                .append("WHERE ").append("enfant_id = ").append(id);

        String query = sb.toString();
        ResultSet resultSet = session.execute(query);
        List<User> parents = new ArrayList<>();
        resultSet.forEach(r -> parents.add(new User(r.getString(("user_token")),r.getString("name"))));
        return parents;
    }
}


