package fr.uge.database;

import com.datastax.driver.core.ResultSet;
import com.datastax.driver.core.Session;
import fr.uge.database.repository.*;
import fr.uge.database.table.Alert;
import fr.uge.database.table.Parent;
import fr.uge.database.table.Position;
import fr.uge.database.table.User;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;
import java.util.stream.Collectors;

public class CreateDB {
    private KeyspaceRepository schemaRepository;
    private Session session;
    private CassandraConnector client;
    private UserRepository userRepository;
    private AlertRepository alertRepository;
    private MessageRepository messageRepository;
    private ParentRepository parentRepository;
    private PositionRepository positionRepository;
    private final String KEYSPACE_NAME = "service";

    public void connect() {
        client = new CassandraConnector();
        client.connect("localhost", 9042);
        this.session = client.getSession();
        schemaRepository = new KeyspaceRepository(session);
        userRepository = new UserRepository(session);
        messageRepository = new MessageRepository(session);
        parentRepository = new ParentRepository(session);
        positionRepository = new PositionRepository(session);
        alertRepository = new AlertRepository(session);
    }
    public Session getSession(){
        return session;
    }
    public void close(){
        client.close();
    }

    public List<String> whenCreatingAKeyspace_thenCreated() {
        schemaRepository.createKeyspace(KEYSPACE_NAME, "SimpleStrategy", 1);

        ResultSet result = session.execute("SELECT * FROM system_schema.keyspaces;");

        return result.all()
                .stream()
                .filter(r -> r.getString(0).equals(KEYSPACE_NAME.toLowerCase()))
                .map(r -> r.getString(0))
                .collect(Collectors.toList());
    }

    public void createAllTable() {
        userRepository.createTable();
        alertRepository.createTable();
        messageRepository.createTable();
        parentRepository.createTable();
        positionRepository.createTable();
    }

    public void insertValues() {

        User user1 = new User("1", "Cassandra");
        User user2 = new User("2", "Valentine");
        User user3 = new User("3", "Michel");
        User user4 = new User("4", "Arthur");
        User user5 = new User("5", "Myriam");

        List<UUID> listParent1 = new ArrayList<>();
        listParent1.add(user3.getUser_id());

        List<UUID> listParent2 = new ArrayList<>();
        listParent2.add(user4.getUser_id());
        listParent2.add(user5.getUser_id());

        Parent parent1 = new Parent(user1.getUser_id(), listParent1);
        Parent parent2 = new Parent(user2.getUser_id(), listParent2);

        Position position1 = new Position(new double[]{8, 5});
        Position position2 = new Position(new double[]{10, 5});
        Position position3 = new Position(new double[]{1, 5});
        Position position4 = new Position(new double[]{6, 3});
        Position position5 = new Position(new double[]{15, 4});

        userRepository.insert(user1);
        userRepository.insert(user2);
        userRepository.insert(user3);
        userRepository.insert(user4);
        userRepository.insert(user5);

        parentRepository.insert(parent1);
        parentRepository.insert(parent2);

        positionRepository.insert(position1);
        positionRepository.insert(position2);
        positionRepository.insert(position3);
        positionRepository.insert(position4);
        positionRepository.insert(position5);

        Alert alert1 = new Alert("TRACKING", position1.getPosition_id(),  user1.getUser_id() ,System.currentTimeMillis(), session);
        Alert alert2 = new Alert("ZONEOUT", position2.getPosition_id() ,user2.getUser_id(),  System.currentTimeMillis(), session);
        Alert alert3 = new Alert("TRACKING", position3.getPosition_id() ,user2.getUser_id(),  System.currentTimeMillis(), session);
        Alert alert4 = new Alert("TRACKING", position4.getPosition_id() ,user2.getUser_id(),  System.currentTimeMillis(), session);
        Alert alert5 = new Alert("TRACKING", position1.getPosition_id() ,user2.getUser_id(),  System.currentTimeMillis(), session);
        Alert alert6 = new Alert("ZONEOUT", position5.getPosition_id() ,user2.getUser_id(),  System.currentTimeMillis(), session);

        alertRepository.insert(alert1);
        alertRepository.insert(alert2);
        alertRepository.insert(alert3);
        alertRepository.insert(alert4);
        alertRepository.insert(alert5);
        alertRepository.insert(alert6);
    }

    public static void main(String[] args) {
        var create = new CreateDB();
        create.connect();
        create.whenCreatingAKeyspace_thenCreated();
        create.createAllTable();
        for(int i = 0; i < 10; i++){
            create.insertValues();
        }
        create.close();
    }
}
