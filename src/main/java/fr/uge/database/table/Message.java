package fr.uge.database.table;

import java.util.UUID;
import com.fasterxml.jackson.annotation.JsonProperty;

public class Message {
    @JsonProperty
    private final UUID message_id;
    @JsonProperty
    private final String message;
    @JsonProperty
    private final UUID position_id;
    @JsonProperty
    private final UUID from_id;
    @JsonProperty
    private final long timestamp;
    @JsonProperty
    private final long server_timestamp;

    public Message(String message, UUID position_id, UUID from_id, long timestamp) {
        message_id = UUID.randomUUID();
        this.message = message;
        this.position_id = position_id;
        this.from_id = from_id;
        this.timestamp = timestamp;
        this.server_timestamp = System.currentTimeMillis();
    }

    public UUID getMessage_id() {
        return message_id;
    }

    public String getMessage() {
        return message;
    }

    public UUID getPosition_id() {
        return position_id;
    }
    
    public UUID getFrom_id() {
        return from_id;
    }

    public long getTimestamp() {
        return timestamp;
    }

    public long getServerTimestamp() {
        return server_timestamp;
    }
}
