package fr.uge.database.table;


import com.datastax.driver.core.Session;
import fr.uge.database.repository.PositionRepository;
import fr.uge.database.repository.UserRepository;

import java.util.Optional;
import java.util.UUID;

public class Alert {
    private final UUID alert_id;
    private final String reason;
    private final Optional<Position> position;
    private final Optional<User> user;
    private final long timestamp;
    private final long server_timestamp;


    public Alert(String reason, UUID position_id, UUID from_id, long timestamp, Session s) {
        this(UUID.randomUUID(),reason,position_id,from_id,timestamp,s);
    }
    public Alert(UUID uuid,String reason, UUID position_id, UUID from_id, long timestamp, Session s) {
        alert_id = uuid;
        this.reason = reason;
        this.position = new PositionRepository(s).getFromId(position_id);
        this.user = new UserRepository(s).getFromId(from_id);
        this.timestamp = timestamp;
        this.server_timestamp = System.currentTimeMillis();
    }

    public UUID getAlert_id() {
        return alert_id;
    }

    public String getReason() {
        return reason;
    }

    public Position getPosition() {
        return position.get();
    }
    
    public User getUser() {
        return user.get();
    }

    public long getTimestamp() {
        return timestamp;
    }

    public long getServerTimestamp() {
        return server_timestamp;
    }
}
