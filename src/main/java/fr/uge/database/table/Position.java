package fr.uge.database.table;

import java.util.Arrays;
import java.util.List;
import java.util.UUID;

public class Position {
    private UUID position_id;
    private double coordinates[];

    public Position(double[] coordinates) {
        this(UUID.randomUUID(),coordinates);
    }
    public Position(UUID uuid, double[] coordinates){
        position_id = uuid;
        this.coordinates = coordinates.clone();
    }
    public UUID getPosition_id() {
        return position_id;
    }

    public double[] getCoordinates() {
        return coordinates.clone();
    }
}
