package fr.uge.database.table;

import java.util.List;
import java.util.UUID;

public class Parent {
    private UUID enfant_id;
    private List<UUID> parent_id;

    public Parent(UUID enfant_id,List<UUID> parent_id) {
        this.enfant_id = enfant_id;
        this.parent_id = List.copyOf(parent_id);
    }

    public List<UUID> getParent_id() {
        return parent_id;
    }

    public UUID getEnfant_id() {
        return enfant_id;
    }
}
