package fr.uge.database.table;

import java.util.UUID;

public class User {
    private UUID user_id;
    private String user_token;
    private String name;

    public User(String user_token, String name){
        this(UUID.randomUUID(),user_token,name);
    }
    public User(UUID uuid, String user_token, String name){
        this.user_id = uuid;
        this.user_token = user_token;
        this.name = name;
    }

    public UUID getUser_id() {
        return user_id;
    }

    public String getUser_token() {
        return user_token;
    }

    public String getName() {
        return name;
    }
}
