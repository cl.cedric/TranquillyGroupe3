package fr.uge.database;

import com.datastax.driver.core.ResultSet;
import com.datastax.driver.core.Row;
import com.datastax.driver.core.Session;

import java.util.Date;

public class TrackService {

    private final String KEYSPACE_NAME = "service";
    private Session session;

    public TrackService(Session session) {
        this.session = session;
    }

    public String trackService(String user_id) {
        StringBuilder sb = new StringBuilder("SELECT * FROM ")
                .append(KEYSPACE_NAME).append(".").append("Alert")
                .append(" WHERE ").append("from_id = ").append(user_id).append(" ALLOW FILTERING;");
        String query = sb.toString();
        ResultSet resultSet = session.execute(query);
        Row row = resultSet.one();
        System.out.println(row.getLong("timestamp"));
        StringBuilder sb1 = new StringBuilder("SELECT * FROM ")
                .append(KEYSPACE_NAME).append(".").append("User")
                .append(" WHERE ").append("user_id = ").append(user_id);
        String query1 = sb1.toString();
        ResultSet resultSet1 = session.execute(query1);
        Row row1 = resultSet1.one();

        StringBuilder sb2 = new StringBuilder("SELECT * FROM ")
                .append(KEYSPACE_NAME).append(".").append("Position")
                .append(" WHERE ").append("position_id = ").append(row.getObject("position_id"));
        String query2 = sb2.toString();
        ResultSet resultSet2 = session.execute(query2);
        Row row2 = resultSet2.one();


        if(query != null) {
            return "200 OK : " + user_id +" "+ row1.getString("name") +" "+ new Date(row.getLong("timestamp")) +" "+ new Date(row.getLong("server_timestamp"))+" ["+ row2.getDouble("coordinates_x")+","+ row2.getDouble("coordinates_y")+"]";
        } else {
            return "404 NOT FOUND";
        }
    }
}
