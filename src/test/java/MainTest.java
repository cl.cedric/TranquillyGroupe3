import com.fasterxml.jackson.databind.JsonDeserializer;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.JsonSerializer;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.deser.std.StringDeserializer;
import com.fasterxml.jackson.databind.node.ObjectNode;
import com.fasterxml.jackson.databind.ser.std.UUIDSerializer;
import fr.uge.database.CassandraConnector;
import fr.uge.database.KafkaManager;
import fr.uge.database.KeyspaceRepository;
import fr.uge.database.CreateDB;
import fr.uge.database.repository.UserRepository;
import com.datastax.driver.core.ColumnDefinitions;
import com.datastax.driver.core.ResultSet;
import com.datastax.driver.core.Session;
import org.apache.kafka.clients.consumer.ConsumerConfig;
import org.apache.kafka.clients.consumer.ConsumerRecords;
import org.apache.kafka.clients.consumer.KafkaConsumer;
import org.apache.kafka.clients.producer.KafkaProducer;
import org.apache.kafka.clients.producer.ProducerConfig;
import org.apache.kafka.clients.producer.ProducerRecord;
import org.apache.kafka.common.KafkaException;
import org.junit.Test;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.TestInstance;

import java.io.IOException;
import java.time.Duration;
import java.util.*;
import java.util.stream.Collectors;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

@TestInstance(TestInstance.Lifecycle.PER_CLASS)
public class MainTest {
    private KeyspaceRepository schemaRepository;
    private Session session;
    private CassandraConnector client;
    private UserRepository userRepository;
    private final String KEYSPACE_NAME = "service";

    @BeforeAll
    void connect() {
        client = new CassandraConnector();
        client.connect("localhost", 9042);
        this.session = client.getSession();
        schemaRepository = new KeyspaceRepository(session);
        userRepository = new UserRepository(session);
    }

    @AfterAll
    void close(){
        client.close();
    }

    private static final String ALERT_TOPIC_IN = "alertKafkaIn";
    private static final String ALERT_TOPIC_OUT = "alertKafkaOut";

    @Test
    public void myTest() throws IOException {
        CreateDB db = new CreateDB();
        db.connect();
//        db.createAllTable();
//        db.insertValues();
        KafkaManager.ConsumerAlert(ALERT_TOPIC_IN, db.getSession());
        for(int i = 0; i <10; i++){
            producerAlertAPI();
        }
        consumerAlertAPI(ALERT_TOPIC_OUT);
        db.close();
    }

    private static final Properties props, props2;
    static {
        props = new Properties();
        props.put(ProducerConfig.CLIENT_ID_CONFIG, "AlertService");
        props.put(ProducerConfig.BOOTSTRAP_SERVERS_CONFIG, "localhost:9092");
        props.put(ProducerConfig.KEY_SERIALIZER_CLASS_CONFIG, UUIDSerializer.class.getName());
        props.put(ProducerConfig.VALUE_SERIALIZER_CLASS_CONFIG, JsonSerializer.class.getName());
        props2 = new Properties();
        props2.put(ConsumerConfig.CLIENT_ID_CONFIG, "AlertService");
        props2.put(ConsumerConfig.BOOTSTRAP_SERVERS_CONFIG, "localhost:9092");
        props2.put(ConsumerConfig.KEY_DESERIALIZER_CLASS_CONFIG, StringDeserializer.class.getName());
        props2.put(ConsumerConfig.VALUE_DESERIALIZER_CLASS_CONFIG, JsonDeserializer.class.getName());
        props2.put(ConsumerConfig.GROUP_ID_CONFIG, "second_app");
        props2.put(ConsumerConfig.AUTO_OFFSET_RESET_CONFIG,"earliest");
    }

    private void producerAlertAPI() throws IOException {
        ObjectMapper mapper = new ObjectMapper();
        ObjectNode jNode = mapper.createObjectNode();
        double coords[] = {255.0, 255.0};
        jNode.put("coordinates", Arrays.toString(coords));
        jNode.put("timestamp", System.currentTimeMillis());
        jNode.put("reason", "TRACKING");
        var tmp= mapper.readTree(jNode.toString());
        try (KafkaProducer<UUID, JsonNode> producer = new KafkaProducer<UUID, JsonNode>(props)) {
            System.out.println("Start sending messages ...");
            producer.send(new ProducerRecord<UUID,JsonNode>(ALERT_TOPIC_IN, UUID.randomUUID(), tmp));
        } catch (KafkaException e) {
            System.exit(-1);
        }
    }

    private void consumerAlertAPI(String topicName) {
        System.out.println("ConsumerConfig terminé");

        KafkaConsumer<String, JsonNode> consumer = new KafkaConsumer<>(props2);
        consumer.subscribe(Collections.singletonList(topicName));
        ConsumerRecords<String, JsonNode> consumerRecords = consumer.poll(Duration.ofMillis(100));
        consumerRecords.forEach(c -> {
            System.out.println("Received - key : " + c.key() + ", value : " + c.value());
        });
    }


    public void whenCreatingAKeyspace_thenCreated() {
        schemaRepository.createKeyspace(KEYSPACE_NAME, "SimpleStrategy", 1);

        ResultSet result = session.execute("SELECT * FROM system_schema.keyspaces;");

        List<String> matchedKeyspaces = result.all()
                .stream()
                .filter(r -> r.getString(0).equals(KEYSPACE_NAME.toLowerCase()))
                .map(r -> r.getString(0))
                .collect(Collectors.toList());

        assertEquals(matchedKeyspaces.size(), 1);
        assertTrue(matchedKeyspaces.get(0).equals(KEYSPACE_NAME.toLowerCase()));
    }

    public void whenCreatingATable_thenCreatedCorrectly() {
        userRepository.createTable();

        ResultSet result = session.execute(
                "SELECT * FROM " + KEYSPACE_NAME + ".user;");

        List<String> columnNames =
                result.getColumnDefinitions().asList().stream()
                        .map(ColumnDefinitions.Definition::getName)
                        .collect(Collectors.toList());
        System.out.println(columnNames);
        assertEquals(columnNames.size(), 3);
        assertTrue(columnNames.contains("user_id"));
        assertTrue(columnNames.contains("user_token"));
        assertTrue(columnNames.contains("name"));

    }

}
